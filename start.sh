#!/usr/bin/env bash

echo "Descarcam salvari :"
git pull origin master

echo "Salvare initiala "
git add -A .
git commit -m "Save"
git push origin master
echo ""
echo "Salvare completa !"

echo ""
echo ""
echo "Pornim salvarea automata !"
screen -S minecraft -d -m sh -c "cd minecraft; while true; do ./save.sh; echo ''; echo 'SAVE !!!'; pwd; sleep 30m ;  done"

echo ""
echo ""

echo ""
echo ""
echo " Pornim servaru pentru prima data :"
    java -Xmx900M -Xms900M -jar minecraft_server.jar nogui;


echo ""
echo ""
echo "Oprim Salvarea automata :"
screen -X -S test minecraft

echo ""
echo ""
echo "Salvare finala :"

git add -A .
git commit -m "Save"
git push origin master
echo "Done!"
echo ""
